package com.jayanthi.spring.data.redis.spring.data.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class UserRepoClient implements IUserRepoClient {

    private static final String KEY_SEPERATOR = ":";
    private static final int FETCH_COUNT = 1000;

    public static Logger log = LoggerFactory.getLogger(UserRepoClient.class);

    @Autowired
    private RedisConnectionFactory connectionFactory;
    @Autowired
    private ValueOperations<String, String> valueOperations;
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public <T> T get(String uid, String key, Class<T> type) throws IOException {
        String valueJSON = valueOperations.get(buildKey(uid, key));
        return valueJSON == null ? null : objectMapper.readValue(valueJSON, type);
    }

    private void buildMap(List<String> keys, List<String> values, Map<String, String> valueMap) {
        Iterator<String> itrKeys = keys.iterator();
        Iterator<String> itrValues = values.iterator();
        while (itrKeys.hasNext() && itrValues.hasNext()) {
            valueMap.put(itrKeys.next(), itrValues.next());
        }
    }

    @Override
    public Map<String, String> get(String guid) {
        Cursor<byte[]> cursor = connectionFactory.getConnection().scan(ScanOptions.scanOptions().match(guid + KEY_SEPERATOR + "*").build());
        List<String> keys = new ArrayList<>();
        int count = 0;
        Map<String, String> valueMap = new HashMap<>();

        fetch:
        while (cursor.hasNext()) {
            keys.add(new String(cursor.next()));
            if (count == FETCH_COUNT) {
                List<String> values = valueOperations.multiGet(keys);
                buildMap(keys, values, valueMap);
                count = 0;
            }
        }
        return valueMap;
    }

    @Override
    public <T> boolean put(String guid, String key, T value) throws JsonProcessingException {
        valueOperations.set(buildKey(guid, key), objectMapper.writeValueAsString(value));
        return true;
    }

    @Override
    public <T> boolean put(String guid, Map<String, T> sessionEntries) {
        return false;
    }

    private String buildKey(String guid, String key) {
        return guid + KEY_SEPERATOR + key;
    }

    @Override
    public boolean remove(String guid) {
        return false;
    }

    @Override
    public boolean remove(String uid, List<String> keys) {
        List<byte[]> keysInBytes = new ArrayList<>();
        for (String key : keys) {
            keysInBytes.add(key.getBytes());
        }
        connectionFactory.getConnection().del((uid + KEY_SEPERATOR + keys.get(0)).getBytes());
        return true;
    }
}
