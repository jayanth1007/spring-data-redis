package com.jayanthi.spring.data.redis.spring.data.redis;


import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Client for interacting with the user repo. Abstracts underlying store provider.
 */
public interface IUserRepoClient {

    public <T> T get(String uid, String key, Class<T> type) throws IOException;
    public Map<String, String> get(String uid);

    public <T> boolean put(String uid, String key, T value) throws JsonProcessingException;
    public <T> boolean put(String uid, Map<String, T> values);

    public boolean remove(String uid);
    public boolean remove(String uid, List<String> keys);

}
