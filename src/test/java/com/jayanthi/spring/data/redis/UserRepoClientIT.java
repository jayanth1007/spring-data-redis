package com.jayanthi.spring.data.redis;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayanthi.spring.data.redis.spring.data.redis.SessionClientConfiguration;
import com.jayanthi.spring.data.redis.spring.data.redis.UserRepoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import redis.embedded.RedisServer;

import java.io.IOException;
import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

@Test
@ContextConfiguration(classes = {SessionClientConfiguration.class, UserRepoClientIT.Configuration.class})
public class UserRepoClientIT extends AbstractTestNGSpringContextTests {

    private static final String UID = "12345";
    private static final String KEY = "userDetails";

    @Autowired
    private RedisConnectionFactory connectionFactory;
    @Autowired
    private UserRepoClient userRepoClient;
    private RedisServer redisServer;

    @org.testng.annotations.BeforeClass
    public void beforeClass() throws IOException {
        redisServer = new RedisServer(6379);
        redisServer.start();
    }

    @AfterClass
    public void after() {
        redisServer.stop();
    }

    @BeforeMethod
    public void before() {
        connectionFactory.getConnection().flushAll();
    }

    @Test
    public void putAndGetSingleValue() throws Exception {
        User user = new User();
        user.userId = "1";
        user.userName = "test user";
        userRepoClient.put(user.userId, KEY, user);
        assertEquals(user, userRepoClient.get(user.userId, KEY, User.class));
    }

    @Test
    public void removeSingleValue() throws IOException {
        User user = new User();
        user.userId = "1";
        user.userName = "test user";
        userRepoClient.put(user.userId, KEY, user);
        userRepoClient.remove(user.userId, Collections.singletonList(KEY));
        User userFetched = userRepoClient.get(user.userId, KEY, User.class);
        assertNull(userFetched);
    }

    static class User {
        public String userId;
        public String userName;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            User user = (User) o;

            if (userId != null ? !userId.equals(user.userId) : user.userId != null) return false;
            return userName != null ? userName.equals(user.userName) : user.userName == null;

        }

        @Override
        public int hashCode() {
            int result = userId != null ? userId.hashCode() : 0;
            result = 31 * result + (userName != null ? userName.hashCode() : 0);
            return result;
        }
    }

    @org.springframework.context.annotation.Configuration
    static class Configuration {

        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }
    }

}